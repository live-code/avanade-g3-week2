import { Component } from '@angular/core';
import {PrimeNGConfig} from "primeng/api";

@Component({
  selector: 'ava-root',
  template: `
    <button routerLink="login">login</button>
    <button routerLink="catalog">catalog</button>
    <button routerLink="users">users</button>
    <button routerLink="demo-components">components 1</button>
    <button routerLink="demo2-components">components 2</button>
    <button routerLink="demo-directives">directives</button>
    <button routerLink="demo-pipes">pipes</button>

    <hr>

    <router-outlet></router-outlet>
  `,
  styles: []
})
export class AppComponent {
  constructor(private primengConfig: PrimeNGConfig) {}

  ngOnInit() {
    this.primengConfig.ripple = true;
  }

}
