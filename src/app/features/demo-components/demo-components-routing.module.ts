import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DemoComponentsComponent } from './demo-components.component';

const routes: Routes = [{ path: '', component: DemoComponentsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoComponentsRoutingModule { }
