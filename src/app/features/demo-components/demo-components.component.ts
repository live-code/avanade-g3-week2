import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ava-demo-components',
  template: `
   <ava-hello-lifecycle
     [title]="myName"
     [color]="myColor"
   ></ava-hello-lifecycle>

   <button (click)="myName = 'Fabio'">Fabio</button>
   <button (click)="myName = 'Ciccio'">ciccio</button>
   <button (click)="myColor = 'green'">green</button>
   <button (click)="myColor = 'pink'">pink</button>


  `,
  styles: [
  ]
})
export class DemoComponentsComponent implements OnInit {
  myName: string | null = null;
  myColor: string | null = null;

  constructor() {
    setTimeout(() => {
      this.myName = 'Pippo';
    }, 1000)
  }

  ngOnInit(): void {
  }

}
