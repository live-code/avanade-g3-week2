import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoComponentsRoutingModule } from './demo-components-routing.module';
import { DemoComponentsComponent } from './demo-components.component';
import {SharedModule} from "../../shared/shared.module";


@NgModule({
  declarations: [
    DemoComponentsComponent
  ],
  imports: [
    CommonModule,
    DemoComponentsRoutingModule,
    SharedModule
  ]
})
export class DemoComponentsModule { }
