import {Component, ElementRef, Injectable, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'ava-demo-directives',
  template: `

    <button avaIfLogged>go to admin1</button>

    <button *avaIfSignin>go to admin2</button>
    <button (click)="auth.isLogged = false">logout</button>

    Visita il sito <span avaUrl="http://www.google.com">google</span>



    <ul class="list-group">
        <ava-meteo [city]="cityName"></ava-meteo>
    </ul>

    <button (click)="cityName = 'mondello'">mondello</button>
    <div [avaPad]="value">ciccio</div>
    <button (click)="value = value + 1">+</button>

    <button (click)="doSomething()">do Something</button>
    <div [avaAlert]>Failed</div>
    <div avaAlert="success">Post Created 1</div>
    <div [avaAlert]="'success'">Post Created 2</div>

    <div [avaBorder]="12" type="dotted" color="cyan">xxxx</div>


  `,
})
export class DemoDirectivesComponent {

  value: number = 1;
  cityName = 'milan';

  constructor(public auth: AuthService) {
  }
  doSomething() {

  }
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLogged = true;
}
