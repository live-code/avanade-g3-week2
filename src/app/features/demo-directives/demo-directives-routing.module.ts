import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DemoDirectivesComponent } from './demo-directives.component';

const routes: Routes = [{ path: '', component: DemoDirectivesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoDirectivesRoutingModule { }
