import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoDirectivesRoutingModule } from './demo-directives-routing.module';
import { DemoDirectivesComponent } from './demo-directives.component';
import {SharedModule} from "../../shared/shared.module";


@NgModule({
  declarations: [
    DemoDirectivesComponent
  ],
  imports: [
    CommonModule,
    DemoDirectivesRoutingModule,
    SharedModule
  ]
})
export class DemoDirectivesModule { }
