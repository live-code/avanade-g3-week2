import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Demo2ComponentsRoutingModule } from './demo2-components-routing.module';
import { Demo2ComponentsComponent } from './demo2-components.component';
import {SharedModule} from "../../shared/shared.module";


@NgModule({
  declarations: [
    Demo2ComponentsComponent
  ],
    imports: [
        CommonModule,
        Demo2ComponentsRoutingModule,
        SharedModule
    ]
})
export class Demo2ComponentsModule { }
