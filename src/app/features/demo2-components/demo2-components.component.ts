import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ava-demo2-components',
  template: `
    <ava-google-map
      [coords]="myPosition"
      [zoom]="myZoom"
    ></ava-google-map>

    <button (click)="myPosition = '33,12'">33,12</button>
    <button (click)="myPosition = '34,13'">34,13</button>
    <button (click)="myZoom = myZoom - 1">-</button>
    <button (click)="myZoom = myZoom + 1">+</button>
  `,
  styles: [
  ]
})
export class Demo2ComponentsComponent implements OnInit {
  myPosition: string | null = null;
  myZoom: number = 4;

  constructor() {
    this.myPosition = '43,12';

    setTimeout(() => {
      this.myPosition = '53,15';
    }, 3000)
  }

  ngOnInit(): void {
  }

}
