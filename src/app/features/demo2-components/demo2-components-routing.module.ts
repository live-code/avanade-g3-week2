import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Demo2ComponentsComponent } from './demo2-components.component';

const routes: Routes = [{ path: '', component: Demo2ComponentsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Demo2ComponentsRoutingModule { }
