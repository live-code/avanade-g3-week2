import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatalogRoutingModule } from './catalog-routing.module';
import { CatalogComponent } from './catalog.component';
import { CatalogListComponent } from './components/catalog-list.component';
import {SharedModule} from "../../shared/shared.module";


@NgModule({
  declarations: [
    CatalogComponent,
    CatalogListComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    CatalogRoutingModule
  ]
})
export class CatalogModule { }
