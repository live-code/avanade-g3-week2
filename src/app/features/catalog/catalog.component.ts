import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ava-catalog',
  template: `
    <p>
      catalog works!
    </p>

    <ava-card></ava-card>
    <ava-tabbar></ava-tabbar>
  `,
  styles: [
  ]
})
export class CatalogComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
