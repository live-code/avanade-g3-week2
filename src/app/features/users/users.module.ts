import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import {ButtonModule} from "primeng/button";
import {RippleModule} from "primeng/ripple";
import {DataViewModule} from "primeng/dataview";
import {AccordionModule} from "primeng/accordion";
import {DialogModule} from "primeng/dialog";


@NgModule({
  declarations: [
    UsersComponent
  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    ButtonModule,
    RippleModule,
    DataViewModule,
    AccordionModule,
    DialogModule
  ]
})
export class UsersModule { }
