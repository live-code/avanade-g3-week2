import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";

interface User {
  id: number;
  name: string;
  username: string;
}
@Component({
  selector: 'ava-users',
  template: `

    <p-dialog [header]="activeUser?.username || ''" [(visible)]="!!activeUser">
      <h1>{{activeUser?.name}}</h1>
      <h1>{{activeUser?.username}}</h1>
    </p-dialog>

    <p-accordion>
      <p-accordionTab [header]="user.name" *ngFor="let user of users">
        {{user.username}}
        <button (click)="showDetails(user)" pRipple pButton type="button" label="Click" >A</button>
      </p-accordionTab>
    </p-accordion>

  `,
  styles: [
  ]
})
export class UsersComponent  {
  users: User[] | null = null;
  activeUser: User | null = null;

  constructor(private http: HttpClient) {
    http.get<User[]>('https://jsonplaceholder.typicode.com/users')
      .subscribe(res => this.users = res)
  }

  showDetails(user: User) {
    this.activeUser = user;
  }
}
