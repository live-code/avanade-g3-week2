import { Component, OnInit } from '@angular/core';
import {getGigabyte} from "../../shared/utils/my-utils";
import {GmapPipe} from "../../shared/pipes/gmap.pipe";

export type Gender = 'F' | 'M' | '';

@Component({
  selector: 'ava-demo-pipes',
  template: `
    {{ memory / 1000}} Gb
    {{ getGb(memory)}} Gb
    <div [innerHTML]="memory | toGigabyte: true">xxx</div>
    {{ memory | toGigabyte: false}} <span>Gigabyte</span>

    {{data | configPipe}}
    <button (click)="memory = 8000">change memory</button>
    <button (click)="data.name = 'ciccio'">change config</button>
    <button (click)="nothing()">nothing</button>

    <hr>

    <img [src]="'milan' | gmap: 10" alt="">
    <hr>
    <span *ngFor="let star of (stars | intToArray)">✩</span>

    <hr>
    <h1>{{gender}}</h1>

    <input type="text" [(ngModel)]="searchName">
    <select [(ngModel)]="gender">
      <option value="">select gender</option>
      <option value="M">male</option>
      <option value="F">female</option>
    </select>

    <hr>
    <form (submit)="filterHandler(f.value)" #f="ngForm">
      <select ngModel name="gender">
        <option value="">select gender</option>
        <option value="M">male</option>
        <option value="F">female</option>
      </select>

      <input type="text" placeholder="name" ngModel name="name">
      <button type="submit">ok</button>
    </form>

    <select (change)="changeGenderHandler($event)">
      <option value="">select gender</option>
      <option value="M">male</option>
      <option value="F">female</option>
    </select>


    <hr>

    <li *ngFor="let user of (users | filterByGender: gender | filterByName: searchName )">
      {{user.name}} - {{user.gender}}
    </li>
  `,
  styles: [
  ]
})
export class DemoPipesComponent  {
  searchName = ''
  gender: Gender = ''
  city = 'Palermo';
  stars = 3;
  users: any[] = [
    {id: 123, name: 'Fabio', gender: 'M'},
    {id: 1233, name: 'Paola', gender: 'F'},
    {id: 1233, name: 'Ciccio', gender: 'M'},
  ]


  memory = 6000;
  data = { value: 123, name: 'Fabio'}

  constructor() {
    console.log(new GmapPipe().transform('milano'))
  }

  getGb(val: number) {
    console.log('getGb method')
    return getGigabyte(val)
  }

  nothing() {

  }

  filterHandler(formData: any) {
    console.log(formData)
    this.gender = formData.gender;
  }

  changeGenderHandler(event: Event) {
    this.gender = (event.target as HTMLSelectElement).value as Gender;
  }
}
