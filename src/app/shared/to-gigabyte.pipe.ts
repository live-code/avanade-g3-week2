import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
  name: 'toGigabyte'
})
export class ToGigabytePipe implements PipeTransform {
  transform(value: number, showLabel: boolean): string {
    console.log(value, showLabel)
    const gb = value / 1000;
    return showLabel ? `${gb} Gb` : `${gb}`;
  }

}
