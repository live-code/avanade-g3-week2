import {Directive, HostBinding, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[avaUrl]'
})
export class UrlDirective {
  @Input() avaUrl!: string
  @HostBinding('style.cursor') cursor = 'pointer'
  @HostBinding('style.text-decoration') deco = 'underline'
  @HostBinding('style.color') color = 'blue'

  @HostListener('click')
  clickMe() {
    window.open(this.avaUrl)
  }

}
