import {Directive, HostBinding, Optional, TemplateRef} from '@angular/core';
import {AuthService} from "../../features/demo-directives/demo-directives.component";

@Directive({
  selector: '[avaIfLogged]'
})
export class IfLoggedDirective {
  @HostBinding('style.display') get display() {
    return this.authService.isLogged ? null : 'none'
  }
  constructor(
    private authService: AuthService,
    @Optional() private tpl: TemplateRef<any>
  ) {
    console.log(tpl)
  }

}
