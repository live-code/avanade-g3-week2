import {Directive, HostBinding, Input} from '@angular/core';

@Directive({
  selector: '[avaBorder]'
})
export class BorderDirective {
  @Input() avaBorder: number = 1;
  @Input() type: 'solid' | 'dotted' | 'dashed' = 'solid';
  @Input() color: string = 'black';

  @HostBinding('style.border') get border() {
    return `${this.avaBorder}px ${this.type} ${this.color} `
  }

}
