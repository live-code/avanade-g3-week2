import {Directive, HostBinding, Input} from '@angular/core';

@Directive({
  selector: '[avaAlert]'
})
export class AlertDirective {
  @Input() avaAlert?: 'danger' | 'success' | 'warning' = 'danger'
  @HostBinding() get className() {
    return `alert alert-${this.avaAlert} `
  }

}
