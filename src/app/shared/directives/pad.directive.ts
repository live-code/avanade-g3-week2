
import {Directive, ElementRef, HostBinding, Input, OnInit, Renderer2} from "@angular/core";

@Directive({
  selector: '[avaPad]'
})
export class PadDirective {
  @Input() set avaPad(value: number) {
    console.log(value)
    this.renderer.setStyle(this.el.nativeElement, 'padding', `${value * 10}px`)
    // this.el.nativeElement.style.padding = `${value * 10}px`
  }

  constructor(
    private el: ElementRef,
    private renderer: Renderer2
  ) {
    console.log(el.nativeElement)
  }


}



// document.getElementById('div').className = 'alert'
// document.getElementById('div').innerHTML = 'ciao ciao'
// document.getElementById('div').style.fontSize = '20px'
