import {Component, HostBinding, Input, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Meteo} from "./meteo";

@Component({
  selector: 'ava-meteo',
  template: `
    <li *ngIf="data">{{data.main.temp}}°</li>
  `,
})
export class MeteoComponent implements OnInit {
  @HostBinding() className = 'list-group-item';

  data: Meteo | null = null;

  @Input() set city(value: string) {
    this.http.get<Meteo>(`http://api.openweathermap.org/data/2.5/weather?q=${value}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
      .subscribe(res => {
        this.data = res;
      })
  }

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

}
