import {AfterViewInit, Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';

@Component({
  selector: 'ava-google-map',
  template: `
    <div #host style="height: 300px"></div>
  `,
})
export class GoogleMapComponent implements OnChanges {
  @Input() coords: string | null = null; // 43,12
  @Input() zoom: number = 5;

  @ViewChild('host', { static: true }) host!: ElementRef<HTMLDivElement>
  map!: google.maps.Map;

  init(): void {
    this.map = new google.maps.Map(this.host.nativeElement, {
      center: { lat: -34.397, lng: 150.644 },
      zoom: this.zoom,
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    // init
    if(changes.coords?.firstChange) {
      this.init();
    }

    // set center
    if(changes.coords) {
     this.setCenter(changes.coords.currentValue)
    }

    // set zoom
    if(changes.zoom) {
      this.setZoom(changes.zoom.currentValue)
    }
  }



  setCenter(coords: string) {
    const myLatlng = new google.maps.LatLng(
      +coords.split(',')[0],
      +coords.split(',')[1],
    );
    this.map.panTo(myLatlng);
  }

  setZoom(value: number) {
    this.map.setZoom(value)
  }
}

