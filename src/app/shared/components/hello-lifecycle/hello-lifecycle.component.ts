import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  ViewChild
} from '@angular/core';

@Component({
  selector: 'ava-hello-lifecycle',
  template: `
    <h1>hello {{title}} {{color}} </h1>
    <input
      value="ciao a tutti"
      #nameInput type="text" placeholder="write something">

    <div #host></div>
  `,
  styles: [
  ]
})
export class HelloLifecycleComponent implements OnInit, AfterViewInit, OnChanges, OnDestroy {
  @Input() title: string | null = null;
  @Input() color: string  | null = 'red';
  @ViewChild('nameInput') input!: ElementRef<HTMLInputElement>;
  @ViewChild('host') host!: ElementRef<HTMLDivElement>;

  ngOnChanges(changes: SimpleChanges): void {
    console.log('ngOnChanges', changes)
    if (changes.title) {
      console.log('do something with title', changes.title.currentValue, this.color)
    }
  }

  ngOnInit(): void {
    console.log('ngOnInit', this.title, this.color)
  }

  ngAfterViewInit(): void {
    console.log(this.input.nativeElement.value)
    console.log(this.host.nativeElement)
    this.input.nativeElement.focus()
  }

  ngOnDestroy(): void {
    console.log('destroy')
  }
}
