import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './components/card/card.component';
import { TabbarComponent } from './components/tabbar/tabbar.component';
import {PadDirective} from "./directives/pad.directive";
import { AlertDirective } from './directives/alert.directive';
import { BorderDirective } from './directives/border.directive';
import { MeteoComponent } from './components/meteo/meteo.component';
import { UrlDirective } from './directives/url.directive';
import { IfLoggedDirective } from './directives/if-logged.directive';
import { IfSigninDirective } from './directives/if-signin.directive';
import {ToGigabytePipe} from "./to-gigabyte.pipe";
import { ConfigPipePipe } from './pipes/config-pipe.pipe';
import { GmapPipe } from './pipes/gmap.pipe';
import { IntToArrayPipe } from './pipes/int-to-array.pipe';
import { FilterByGenderPipe } from './pipes/filter-by-gender.pipe';
import { FilterByNamePipe } from './pipes/filter-by-name.pipe';
import { HelloLifecycleComponent } from './components/hello-lifecycle/hello-lifecycle.component';
import { GoogleMapComponent } from './components/google-map/google-map.component';



@NgModule({
  declarations: [
    CardComponent,
    TabbarComponent,
    PadDirective,
    AlertDirective,
    BorderDirective,
    MeteoComponent,
    UrlDirective,
    IfLoggedDirective,
    IfSigninDirective,
    ToGigabytePipe,
    ConfigPipePipe,
    GmapPipe,
    IntToArrayPipe,
    FilterByGenderPipe,
    FilterByNamePipe,
    HelloLifecycleComponent,
    GoogleMapComponent
  ],
  imports: [
    CommonModule
  ],
    exports: [
        CardComponent,
        TabbarComponent,
        PadDirective,
        AlertDirective,
        BorderDirective,
        MeteoComponent,
        IfLoggedDirective,
        UrlDirective,
        IfSigninDirective,
        ToGigabytePipe,
        ConfigPipePipe,
        IntToArrayPipe,
        GmapPipe,
        FilterByGenderPipe,
        FilterByNamePipe,
        HelloLifecycleComponent,
        GoogleMapComponent
    ]
})
export class SharedModule { }
