import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterByName'
})
export class FilterByNamePipe implements PipeTransform {
  transform(items: any[], text: string): any {
    text = text.toLowerCase();
    if (items) {
      return items.filter((item) => {
        const findIndex = item.name.toLowerCase().indexOf(text);
        return findIndex >= 0 ;
      });
    }
  }
}
