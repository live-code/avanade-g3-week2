import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterByGender'
})
export class FilterByGenderPipe implements PipeTransform {

  transform(users: any[], gender: string): any[] {
    if (!gender) {
      return users;
    }
    return users.filter(u => u.gender === gender);
  }

}
