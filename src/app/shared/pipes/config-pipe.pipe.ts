import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'configPipe',
  pure: false
})
export class ConfigPipePipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    console.log(value);
    return JSON.stringify(value, null, 2);
  }

}
