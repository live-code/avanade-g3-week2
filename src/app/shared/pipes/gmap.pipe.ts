import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'gmap'
})
export class GmapPipe implements PipeTransform {

  transform(value: string, zoom: number = 5): unknown {
    return `https://maps.googleapis.com/maps/api/staticmap?center=${value}&zoom=${zoom}&size=200x100&key=AIzaSyBzQurvW6YciltmMcXNZwKo5njyFIn_f8I`;
  }

}
