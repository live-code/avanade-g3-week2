import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) },
  { path: 'catalog', loadChildren: () => import('./features/catalog/catalog.module').then(m => m.CatalogModule) },
  { path: 'users', loadChildren: () => import('./features/users/users.module').then(m => m.UsersModule) },
  { path: '', redirectTo: 'login', pathMatch: 'full'},
  { path: 'demo-directives', loadChildren: () => import('./features/demo-directives/demo-directives.module').then(m => m.DemoDirectivesModule) },
  { path: 'demo-pipes', loadChildren: () => import('./features/demo-pipes/demo-pipes.module').then(m => m.DemoPipesModule) },
  { path: 'demo-components', loadChildren: () => import('./features/demo-components/demo-components.module').then(m => m.DemoComponentsModule) },
  { path: 'demo2-components', loadChildren: () => import('./features/demo2-components/demo2-components.module').then(m => m.Demo2ComponentsModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
